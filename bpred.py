class BranchPredictor:
	def lookup(self, b_addr, b_target, b_opcode,
		is_call, is_return):
		print("TRIED TO LOOKUP");
		print("branch location: %ld" % b_addr)
		print("branch target: %ld" % b_target)
		print("branch opcode: %ld" % b_opcode)
		print("branch is_call: %s" % bool(is_call))
		print("branch is_return: %s" % bool(is_return))
		

	def update(self, b_addr, b_target, taken, pred_taken,
		correct, opcode):

		print("branch location: %ld" % b_addr)
		print("branch target: %ld" % b_target)
		print("branch opcode: %ld" % b_opcode)
		print("branch is_call: %s" % bool(is_call))
		print("branch is_return: %s" % bool(is_return))

		return -1



class BTFN(BranchPredictor):
	def __init__(self):
		print("USING BTFN Predictor");
		super()

	def lookup(self, b_addr, b_target, b_opcode, is_call, is_return):
		super().lookup(b_addr, b_target, b_opcode, is_call, is_return)
		if b_target < b_addr:
			return b_target
		return 0
