class DirectMapped(BranchPredictor):
	def __init__(self):
		self.map = {}

	def lookup(self, b_addr, b_target, b_opcode, is_call, is_return):
		key = b_addr % 10000
		if key in self.map:
			print "found key"
			value = self.map[key]
		else:
			self.map[key] = 0
			value = 0

		return value


	def update(self, b_addr, b_target, taken, pred_taken, correct, opcode):
		key = b_addr % 10000
		if key in self.map:
			self.map[key] = taken
		else:
			print "error"
		return 0

test = DirectMapped()

test.lookup(34333335, 34333335, 4, True, False)
test.lookup(34333335, 34333335, 4, True, False)

