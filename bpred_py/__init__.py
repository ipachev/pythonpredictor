import os.path, pickle
from sklearn.ensemble import RandomForestClassifier

import numpy as np
import time
from collections import deque
import sys  

script_dir = os.path.dirname(__file__)
rel_path = "data.p"
data_file = os.path.join(script_dir, rel_path)

NUM_CACHE_LAST_OPCODES = 10

class BranchPredictor:
    def lookup(self, b_addr, b_target, b_opcode, is_call, is_return):
        print("TRIED TO LOOKUP")
        print("branch location: %ld" % b_addr)
        print("branch target: %ld" % b_target)
        print("branch opcode: %ld" % b_opcode)
        print("branch is_call: %s" % bool(is_call))
        print("branch is_return: %s" % bool(is_return))
        return b_target

    def update(self, b_addr, b_target, taken, pred_taken, correct, b_opcode):
        print("TRIED TO UPDATE")
        print("branch location: %ld" % b_addr)
        print("branch target: %ld" % b_target)
        print("branch opcode: %ld" % b_opcode)
        print("branch taken: %d" % taken)
        print("branch prediction: %d" % pred_taken)
        print("branch correctly predicted: %d" % correct)
        return 0

    def cache_inst(self, opcode):
        print("found instruction %d" % opcode)

    def cleanup(self):
        pass

class BTFN(BranchPredictor):
    def lookup(self, b_addr, b_target, b_opcode, is_call, is_return):
        if b_target < b_addr:
            return b_target
        return 0


class MaxSizeQueue():
    def __init__(self, size):
        self.q = deque(maxlen=size)
        for i in range(size):
            self.q.append(0)

    def put(self, thing):
        self.q.append(thing)

    def to_list(self):
        return list(self.q)

    def __iter__(self):
        return self.q.__iter__()


class TrainingData:
    def __init__(self):
        self.input_data = []
        self.target_data = []

class DataCollector(BranchPredictor):
    def __init__(self):
        print("opening training data file")
        if os.path.isfile(data_file):
            self.file = open(data_file, "rb")
            self.training_data = pickle.load(self.file)
            print("opened file")
        else:
            self.file = None
            self.training_data = TrainingData()
            print("training data not found, building new one")

        self.inst_queue = MaxSizeQueue(NUM_CACHE_LAST_OPCODES)

    def cache_inst(self, opcode):
        print("found instruction %d " % opcode)
        self.inst_queue.put(opcode)

    def lookup(self, b_addr, b_target, b_opcode, is_call, is_return):
        sample = [b_addr - b_target, b_opcode, is_call, is_return]
        sample.extend(self.inst_queue)
        print("found sample %s " % str(sample))
        self.training_data.input_data.append(sample)
        return 0

    def update(self, b_addr, b_target, taken, pred_taken, correct, b_opcode):
        self.training_data.target_data.append(taken)
        return 0


    def cleanup(self):
        if self.file is not None:
            self.file.close()
        self.file = open(data_file, "wb")
        pickle.dump(self.training_data, self.file)
        print("total %d input samples" % len(self.training_data.input_data))
        print("total %d target samples" % len(self.training_data.target_data))
        print("Cleaning up!!!")
        self.file.close()

class TrainingDataReader:
    def __init__(self):
        if os.path.isfile(data_file):
            with open(data_file, mode="rb") as f:
                self.training_data = pickle.load(f)
        else:
            print("ERROR: %s NOT FOUND!\n" % data_file)

    def output(self):
        for i in range(len(self.training_data.input_data)):
            print(self.training_data.input_data[i])
            print("    ")
            print(self.training_data.target_data[i])

def open_training_data():
    if os.path.isfile(data_file):
        with open(data_file, mode="rb") as f:
            training_data = pickle.load(f)
            print("found %d %s" % (len(training_data.input_data), " input samples"))
            print("found %d %s" % (len(training_data.target_data), " target samples"))
        return training_data
    else:
        print("ERROR: %s NOT FOUND!\n" % data_file)
        return None


class RFPredictor(BranchPredictor):
    def __init__(self):
        self.training_data = open_training_data()
        self.classifier = RandomForestClassifier(n_estimators=100)
        # self.classifier = AdaBoostClassifier(n_estimators=10)
        self.classifier.fit(self.training_data.input_data, self.training_data.target_data)
        self.inst_queue = MaxSizeQueue(NUM_CACHE_LAST_OPCODES)

    def cache_inst(self, opcode):
        #print("found instruction %d" % opcode)
        self.inst_queue.put(opcode)
    
    def lookup(self, b_addr, b_target, b_opcode, is_call, is_return):
        #print("last 5 instructions %s " % str(self.inst_queue.to_list()))
        sample = [b_addr - b_target, b_opcode, is_call, is_return]
        sample.extend(self.inst_queue)
        result = self.classifier.predict([sample])
        return b_target if result[0] == 1 else 0

    def update(*args):
        pass
